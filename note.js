//数组：for   for...in
//json: for...in


var obj = {
    a: 15,
    b: 1,
    c: 3,
}
for (i in obj) {
    alert(i + ' = ' + obj[i]);
}


var arr = [12, 3, 4, ]
for (var i = 0; i < arr.length; i++) {
    alert(i + ' = ' + arr[i]);
}
for (i in arr) {
    alert(i + ' = ' + arr[i]);
}