function getStyle(obj, attr) {
    if (obj.currentStyle) {
        return obj.currentStyle[attr];
    } else {
        return getComputedStyle(obj, false)[attr];
    }
} //获得属性
function startMove(obj, attr, iTarget) {
    clearInterval(obj.timer);
    obj.timer = setInterval(function() {
        var iCur = 0;
        if (attr == 'opacity') {
            iCur = parseFloat(getStyle(obj, attr)) * 100; //parsefloat认识小数；
        } else {
            iCur = parseInt(getStyle(obj, attr)); //iCur为取得的属性值//parseint取整，见小数点会停
        }
        var iSpeed = (iTarget - iCur) / 8;
        iSpeed = iSpeed > 0 ? Math.ceil(iSpeed) : Math.floor(iSpeed);
        if (iCur == iTarget) {
            clearInterval(obj.timer);
        } else {
            if (attr == 'opacity') {
                obj.style.filter = 'alpha(opacity =' + (iCur + iSpeed) + ')';
                obj.style.opacity = (iCur + iSpeed) / 100;
                // document.getElementById('txt1').value = obj.style.opacity;
            } else {
                obj.style[attr] = iCur + iSpeed + 'px';
            }
        }
    }, 30)
}